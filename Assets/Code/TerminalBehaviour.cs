﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code
{
    class TerminalBehaviour : MonoBehaviour
    {
        public LayerMask groundLayer;

        bool Interact()
        {
            Vector2 position = transform.position;
            Vector2 direction = Vector2.down;
            float distance = 1.0f;
            Debug.DrawRay(position, direction, Color.green);
            RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, groundLayer);
            if (hit.collider != null)
            {
                return true;
            }
            return false;
        }

        void Start()
        {
            
        }

        void Update()
        {
            if(Interact() && Input.GetKey(KeyCode.E))
            {
                Debug.Log("Colliding");
                //Do Terminal Scene
            }
        }
    }
}
