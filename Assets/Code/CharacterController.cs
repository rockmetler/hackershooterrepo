﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public float movementSpeed;
    public float dodgeValue;
    public Vector3 bulletOffset = new Vector3(0, 0.5f, 0);
    public GameObject bulletPrefab;
    public Vector3 BulletPos;
    public float fireDelay = 0.25f; //subject to change depending on player weapon
    public float thrust;
    public float DestroyTime;
    public static float curhealth = 3;
    public float dodgeDelay;
    private Vector3 moveDirection = Vector3.zero;
    //private Animator animator;
    public LayerMask groundLayer;
    internal KeyCode KeyCode;
    private float cooldownTimer;
    private float dodgeCooldowntimer;
    private int bulletLayer;
    private Vector3 shotDir;

    bool Interact()
    {
        Vector2 position = transform.position;
        Vector2[] directions = {
            Vector2.down,
            Vector2.up,
            Vector2.left,
            Vector2.right
        };
        float distance = 1.0f;
        foreach (var i in directions)
        {
            Debug.DrawRay(position, i, Color.green);
            RaycastHit2D hit = Physics2D.Raycast(position, i, distance, groundLayer);
            if (hit.collider != null)
            {
                return true;
            }
        }

        return false;
    }

    // Use this for initialization
    void Start()
    {
        //animator = GetComponent<//animator>();
        shotDir = Vector3.right;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 cursorInWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //transform.LookAt(cursorInWorldPos);
        var facingDirection = cursorInWorldPos - transform.position;
        var aimAngle = Mathf.Atan2(facingDirection.y, facingDirection.x);
        if (aimAngle < 0f)
        {
            aimAngle = Mathf.PI * 2 + aimAngle;
        }
        var aimDirection = Quaternion.Euler(0, 0, aimAngle * Mathf.Rad2Deg) * Vector2.right;
        Debug.DrawRay(transform.position, aimDirection, Color.green);

        cooldownTimer -= Time.deltaTime;
        dodgeCooldowntimer -= Time.deltaTime;
        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<SpriteRenderer>().flipX = false;
            transform.position += Vector3.right * movementSpeed * Time.deltaTime;
            shotDir = Vector3.right;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * movementSpeed * Time.deltaTime;
            GetComponent<SpriteRenderer>().flipX = true;
            shotDir = Vector3.left;
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * movementSpeed * Time.deltaTime;
            shotDir = Vector3.up;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * movementSpeed * Time.deltaTime;
            shotDir = Vector3.down;
        }
        if (Input.GetButton("Fire1") && cooldownTimer <= 0)
        {
            cooldownTimer = fireDelay;

            Vector3 offset = transform.rotation * bulletOffset;
            cursorInWorldPos.Normalize();
            GameObject bulletGO = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y), transform.rotation);
            bulletGO.layer = bulletLayer;
            bulletGO.GetComponent<Rigidbody2D>().velocity = new Vector3(aimDirection.x, aimDirection.y).normalized * thrust;
            Debug.Log(Vector3.Distance(transform.position + new Vector3(2.0f,2.0f,0),aimDirection));
            Destroy(bulletGO, DestroyTime);
        }
        if (Input.GetKey(KeyCode.Space) && dodgeCooldowntimer <= 0)
        {
            dodgeCooldowntimer = dodgeDelay;
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += Vector3.right * movementSpeed * 4 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position += Vector3.left * movementSpeed * 4 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position += Vector3.down * movementSpeed * 4 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += Vector3.up * movementSpeed * 4 * Time.deltaTime;
            }
        }
        if (curhealth <= 0)
        {
            Death();
        }
    }

    void Death()
    {

    }
}
