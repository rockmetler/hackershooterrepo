﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentHack : MonoBehaviour {

    internal CircleCollider2D triggerArea;
    internal BoxCollider2D windEffect;
    internal GameObject player;
    public LayerMask Layer;

    bool ventSwitch()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 1.0f;
        Debug.DrawRay(position, direction, Color.green);
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, Layer);
        if (hit.collider != null)
        {
            hit.transform.gameObject.transform.position += Vector3.down * Time.deltaTime;
            return true;
        }
        return false;
    }

    // Use this for initialization
    void Start () {
        triggerArea = GetComponent<CircleCollider2D>();
        windEffect = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
        if (triggerArea.OverlapPoint(player.transform.position))
        {
            if (Input.GetKey(KeyCode.E))
            {
                if (ventSwitch())
                {
                    
                }
            }
        }
	}
}
